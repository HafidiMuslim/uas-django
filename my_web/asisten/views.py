from .models import Asisten
from django.shortcuts import render
from django.urls import reverse_lazy
from .utils import Render
from django.views.generic import DetailView, CreateView, UpdateView, DeleteView, View

# Create your views here.

var = {
    'judul' : 'Penyedia Asisten Rumah Tangga',
    'info' : '''Menydiakan asisten rumah tangga yang siap di pekerjakan''',
             
}

def index(self):
	var['asisten'] = Asisten.objects.values('id','nama_pekerja','kategori').order_by('nama_pekerja')
	return render(self,'asisten/index.html',context=var)

class AsistenDetailView(DetailView):
	model = Asisten
	template_name = 'asisten/alat_detail_view.html'

	def get_context_data(self,**kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class AsistenCreateView(CreateView):
	model = Asisten
	fields = '__all__'
	template_name = 'asisten/alat_add.html'

	def get_context_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class AsistenEditView(UpdateView):
	model = Asisten
	fields = ['nama_pekerja','kategori','alamat_rumah','usia','pengalaman','gaji']
	template_name = 'asisten/alat_edit.html'

	def get_context_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class AsistenDeleteView(DeleteView):
	model = Asisten
	template_name = 'asisten/alat_delete.html'
	success_url = reverse_lazy('home_page')

	def get_context_data(self, **kwargs):
		context = var
		context.update(super().get_context_data(**kwargs))
		return context

class AsistenToPdf(View):
	def get(self,request):
		var = {
		'asisten' : Asisten.objects.values('nama_pekerja','kategori','pengalaman','alamat_rumah'),'request':request
		}
		return Render.to_pdf(self,'asisten/alat_to_pdf.html',var)
